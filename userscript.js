// ==UserScript==
// @name        CAI Scripts
// @namespace   http://tampermonkey.net/
// @include     https://beta.character.ai/*
// @require     https://raw.githubusercontent.com/eligrey/FileSaver.js/master/dist/FileSaver.min.js
// @require     https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @grant       GM_xmlhttpRequest
// @version     0.1
// @author      -
// @description Current Features: 1. Download chat log
// ==/UserScript==

function addScript(text) {
  let savedConfig = JSON.parse(localStorage.getItem( "ScriptConfig" ));
  let chatDelay = 0;
  if (savedConfig) { chatDelay = Number(savedConfig.Chat.messageStreamDelay)}
  if (chatDelay <= 0) {
    text = text.replace(/,new Promise\(\(function\(e\)\{return setTimeout\(e,p\)\}\)\)/g, '');
  }
  else {
    text = text.replace(/return setTimeout\(e,p\)/g, `return setTimeout(e,${chatDelay})`);
  }
  // text = text.replace(/"chunks_to_pad",8/g,'"chunks_to_pad",8');
  var newScript = document.createElement('script');
  newScript.type = "text/javascript";
  newScript.textContent = text;
  var head = document.getElementsByTagName('head')[0];
  head.appendChild(newScript);
}

window.addEventListener('beforescriptexecute', (e) => {
  src = e.target.src; // https://beta.character.ai/static/js/main.cef6c92e.js
  if (src.search(/main\.cef6c92e\.js/) != -1) {
      console.log('JS Intercepted!');
      e.preventDefault();
      e.stopPropagation();
      GM_xmlhttpRequest({
          method: "GET",
          url: e.target.src,
          onload: (response) => {
              addScript(response.responseText);
          }
      });
  }
});

( function() {
'use strict';

// Settings
let scriptConfig = {
  Misc: {
    useMarkdownForChatDownload: false,
    enableConvenienceHacks: true,
  },
  Chat: {
    messageStreamDelay: 0,
    showChatCharImage: false,
    chatCharImageSize: 400,
    showChatSceneImage: false,
    chatSceneImageSize: 512,
    chatSceneImageTag: '[Scene]'

  },
  ReplyWindow: {
    showChatReplyWindow: true,
    replyContainerPosition: 'right',
    replyHighlightColor: 'skyblue',
    useReplyRatingColor: false,
    replyContainerWidth: 30 // In percent
  }
}
let savedConfig = localStorage.getItem( "ScriptConfig" );
if (savedConfig) { scriptConfig = JSON.parse(savedConfig);}

let hotkey = {
  Chat: {
    ShowConfig: 'alt-q',
    ClickRetry: 'alt-1',
    ClickRemove: 'alt-2',
    ShowReplyWindow: 'alt-3',
    DisplayChatCharImage: 'alt-4',
    DisplayChatSceneImage: 'alt-5',
    DownloadChat: 'alt-6'
  },
  Edit: {
    EditSave: 'alt-1'
  }
}
let savedHotkey = localStorage.getItem( "ScriptHotkey" );
if (savedHotkey) { hotkey = JSON.parse(savedHotkey); }

let sceneLibrary = {};
let savedScenes = localStorage.getItem( "SceneLibrary" );
if (savedScenes){ sceneLibrary = JSON.parse(savedScenes); }

// Classes to tag injected elements
const cls ={
    Chat: 'chat-inject',
    Menu: 'menu-inject'
}

const DebugLog = (label,item) =>
{
  if (true) {
    console.log(label, item);
  }
}

const buildConfigItems = (items,label) =>
{
    let configItems = document.createElement('div');
    configItems.style = `align-self:center;  width:65%;`;
    for (const [key, value] of Object.entries(items)) {
      let configContainer = document.createElement('div');
      configContainer.style = `display:flex; align-self:center; justify-content:space-between;`;
      let configItem = document.createElement('div');
      configItem.innerHTML = `${key}:`;
      configItem.style = `display:flex; align-content:center; margin:0.25em 0 1em 0;`;

      let inputDiv = configItem.cloneNode();
      let configInput = document.createElement('input');
      inputDiv.appendChild(configInput);
      configInput.classList.add("border");
      configInput.style = `align-self:center; padding:0 1em; height:1.5em; width:10em;`;
      if (typeof value == "number") {
        configInput.setAttribute("type", "number");
        configInput.setAttribute("min", 0);
      }
      else if (typeof value == "boolean") {
        configInput.setAttribute("type", "checkbox");
        configInput.style = `align-self:center; padding:0; height:1em; width:1em;`;
        configInput.checked = value;
      }
      else if (typeof value == "string") {
        configInput.setAttribute("type", "text");
      }
      configInput.value = value;
      configInput.oninput = () => {
        const val = { 'number': Math.max(Number(configInput.value), 0), 'boolean': configInput.checked,'string': configInput.value };
        items[key] = val[typeof value];
        let saveDict = label.includes(tabLabel.hotkey) ? hotkey : scriptConfig;
        if (saveDict == hotkey)
          localStorage.ScriptHotkey = JSON.stringify(saveDict);
        else {
          localStorage.ScriptConfig = JSON.stringify(saveDict);
        }
      }

      configContainer.appendChild(configItem);
      configContainer.appendChild(inputDiv);
      configItems.appendChild(configContainer);
  }
  return configItems;
}

const tabLabel = {
  config: 'Config',
  hotkey: 'Hotkey'
}

const showConfigMenu = (tab) =>
{
  let configMenu = document.getElementById('inject-config-menu');
  if (configMenu) {
    configMenu.remove();
  }
  else {
    configMenu = document.createElement('div');
    configMenu.id = 'inject-config-menu';
    configMenu.style = `position:absolute; top:10%; left:25%; display:flex; flex-direction:column; align:center;
    padding:2em; margin:auto; width:50%; background-color:#fefefe; border: 1px solid #888; z-index: 90000`;
    configMenu.classList.add(cls.Menu);

    let closeButton = document.createElement('Button');
    closeButton.innerHTML = "Close";
    closeButton.classList.add(cls.Menu,"btn","border");
    closeButton.style = "align-self:center; margin-left:0.75em; padding:0.2em; width:10%;";
    closeButton.addEventListener("click", () => { configMenu.remove(); });

    let selectDiv = document.createElement('div');
    selectDiv.style = "display:flex; justify-content:center; width:100%;";
    const setTabButton = (el,label) =>
    {
      el.innerHTML = label;
      el.classList.add(cls.Menu,"btn","border");
      el.style = `align-self:center; margin-left:0.75em; padding:0.2em; width:30%; background-color:${tab == label ? 'lightblue' : ''}`;
      el.addEventListener("click", () => { configMenu.remove(); showConfigMenu(label); } );
      selectDiv.appendChild(el);
    }
    let configSelect = document.createElement('button');
    setTabButton(configSelect, tabLabel.config);
    let hotkeySelect = document.createElement('button');
    setTabButton(hotkeySelect, tabLabel.hotkey);
    configMenu.appendChild(selectDiv);

    const tabDict = { Config: scriptConfig, Hotkey: hotkey }
    const selectDict = tabDict[tab];
    for (const [key, value] of Object.entries(selectDict)) {
      let configTitle = document.createElement('div');
      configTitle.innerHTML = key;
      configTitle.style = `font-size:1.2em; margin-bottom:0.5em; border-bottom: 2px solid white;`;
      configMenu.appendChild( configTitle );
      const typeLabel =
      configMenu.appendChild( buildConfigItems(value,`${key}-${tab}`) );
    }

    configMenu.appendChild(closeButton);
    document.body.appendChild(configMenu);
  }
}

const parseChatHTML = (text) =>
{
  text = text.replaceAll(/<\/p>|<\/li>/gm, '\n'); // newlines
  text = text.replaceAll(/<em>|<\/em>/gm, '*'); // italics
  text = text.replaceAll(/<strong>|<\/strong>/gm, '**'); // bold
  text = text.replaceAll(/<del>|<\/del>/gm, '~~'); // strikethrough
  text = text.replaceAll(/<h([1-9])>/gm, (a,b) => { return "#".repeat(Number(b))+' '; }); // '#'.repeat(parseInt('$1'))
  text = text.replaceAll(/<.*?src="([^"]+).*?>/gm, '![]($1)'); // images
  text = text.replaceAll(/<code>|<\/code>/gm, '`'); // code/highlight
  text = text.replaceAll(/<li>/gm, '- '); // lists
  text = text.replace(/<[^>]*>?/gm, ''); // remove all other html
  text = text.replaceAll(/\n$/gm, ''); // prevent double newlines
  return text;
}

const getChatText = () =>
{
  const messageNodes = document.querySelectorAll('.msg-row');
  const messages = [].slice.call(messageNodes, 0).reverse();
  let chatMessages = '';
  let speakers = [];

  messages.forEach(el => {
    let textElement = el.querySelector('.markdown-wrapper');
    let speakerElement = el.querySelector('span').childNodes[0];
    let text = scriptConfig.Misc.useMarkdownForChatDownload ? parseChatHTML(textElement.innerHTML) : textElement.textContent;
    let speaker = speakerElement.textContent;
    if (!speakers.includes(speaker) && !speaker.includes("@")) { speakers.push(speaker) };
    chatMessages += `\n${speaker}:\n${text}\n`;
  });

  return [chatMessages, speakers];
}

const getCurrentCharacter = () =>
{
  const url = location.href;
  const currentCharacter = url.match(/char=([^&]+)/);
  return currentCharacter[1];
}

const archiveChat = () =>
{
  let chatTitle = document.querySelector('.chattitle').childNodes[1];
  let title = chatTitle.textContent;
  let date = new Date().toISOString().slice(0, 10)
  let time = new Date().toTimeString().slice(0, 8).replaceAll(":", "-");
  let filename = `${date}_${title}_${time}`
  // alert(`File: ${chatMessages}`);
  // console.log(filename + '\n' + chatMessages);
  let chatText = getChatText()
  let header = `// Archived from ${window.location.href} on ${date} at ${time}\n// Conversation between: ${chatText[1].join(", ")}\n`
  const blob = new Blob([header, chatText[0]], {type: "text/plain;charset=utf-8"});
  saveAs(blob, `[CAI]${filename.replaceAll(" ", "_")}.txt`);
}

const setChatImage = (src,size,style,id = 'none') =>
{
    const image = document.createElement("img");
    console.log(`$IMAGE -- ${src}`)
    image.src = src;
    image.height = size;
    if (id != 'none') {image.id = id; };
    image.classList.add(cls.Chat, "border");
    image.style = style + " z-index: 1000; border-radius: 2%;";
    document.body.appendChild(image);
}

const setSceneImage = (src) =>
{
    if (document.getElementById('inject-chat-scene-image')) (duplicate) => { duplicate.remove(); };
    setChatImage(src,scriptConfig.Chat.chatSceneImageSize, "top:0; right:1; position:absolute;", 'inject-chat-scene-image');
    const dictCurrentScene = { current: src };
    sceneLibrary[getCurrentCharacter()] = dictCurrentScene;
    // TODO: Call scene menu onclick
    localStorage.SceneLibrary = JSON.stringify(sceneLibrary);
}

const createChatSceneImage = (tryLoad) =>
{
  // Can use find if latest message is always first. Always true? Otherwise, need filter and select
  const imageMessages = [...document.querySelectorAll('div.markdown-wrapper')].find(el => el.textContent.includes(scriptConfig.Chat.chatSceneImageTag) );
  if (tryLoad && getCurrentCharacter() in sceneLibrary) {
    console.log("LOADING IMAGE");
    setSceneImage(sceneLibrary[getCurrentCharacter()]['current']);
  }
  else if (imageMessages){
    let sceneImageSrc = imageMessages.querySelector('img');
    console.log("SCENE IMAGE", sceneImageSrc);
    if (sceneImageSrc) {
      setSceneImage(sceneImageSrc.src);
    }
  }
}

let charNameTitle = document.getElementsByClassName('chattitle');
const createChatCharImage = () =>
{
  if (!document.getElementById('inject-chat-char-image') && charNameTitle.length > 0) {
    console.log("TEST:", charNameTitle)
    let charName = [...charNameTitle[0].childNodes][1].data;
    let imageSrc = document.querySelector(`img[title='${charName}']`);
    console.log("Image:",`${charName} -- ${imageSrc}`);
    if (imageSrc) {
      let source = imageSrc.src.replace("/80/",`/400/`);
      setChatImage(source, scriptConfig.Chat.chatCharImageSize, "bottom:0; right:1; position:absolute; z-index: 1000;", 'inject-chat-char-image');
    }
  }
}

const toggleChatCharImage = () =>
{
  scriptConfig.Chat.showChatCharImage = !scriptConfig.Chat.showChatCharImage
  if (!scriptConfig.Chat.showChatCharImage) {
    let images = document.querySelectorAll(`img.${cls.Chat}`);
    images.forEach(el => { el.remove(); });
  }
  else {
    createChatCharImage();
  }
}

const toggleSceneImage = () =>
{
  scriptConfig.Chat.showChatSceneImage = !scriptConfig.Chat.showChatSceneImage
  if (!scriptConfig.Chat.showChatSceneImage) {
    let images = document.querySelectorAll(`img.${cls.Chat}`);
    images.forEach(el => { el.remove(); });
  }
  else {
    createChatSceneImage(true);
  }
}

const clickDropdownElement = (buttonText, clickDropup=false) =>
{
  let dropdown = document.querySelectorAll('span[data-toggle="dropdown"]');
  dropdown = clickDropup ? dropdown[1] : dropdown[0];
  dropdown.click()
  let button = [...document.querySelectorAll('button.dropdown-item')].find(el => el.textContent === buttonText);
  button.click();
}

let currentReplies = [];
let reactSwipeContainer = document.getElementsByClassName('react-swipeable-view-container');
let recentReplies = document.getElementsByClassName('markdown-wrapper-last-msg');
let replyContainers = document.getElementsByClassName('reply-container-inject');

const replyClick = (text) =>
{
  const ke = new KeyboardEvent('keydown', { bubbles: true, cancelable: true, keyCode: 39 });
  const swipeView = [...reactSwipeContainer][0];
  for (let i = 0; i < 8; i++) {
    let visibleReply = swipeView.querySelector('div[aria-hidden="false"]').querySelector('div.markdown-wrapper');
    if (visibleReply.textContent === text) {
      break;
    }
    else {
      document.body.dispatchEvent(ke);
    }
  }
};

const ratingColors = { Terrible: 'red', Bad: 'orange', Good: 'blue', Fantastic: 'green'}

const ratingClick = (value, el) =>
{
  // Need to manually navigate and update rating button. Can't reuse ref from button creation.
  const swipeView = [...reactSwipeContainer][0];
  let visibleRating = swipeView.querySelector('div[aria-hidden="false"]').querySelector('div.annotation-buttons-container').childNodes[0].childNodes;
  let selectedRating = [...visibleRating].find(el => el.title === value);
  selectedRating.click();
  let visibileRatingButtons = [...visibleRating];
  let ratingButtons = [...el.parentElement.childNodes];
  ratingButtons.forEach( (el,i) => {
    if (el.tagName == 'DIV') {
      if (scriptConfig.ReplyWindow.useReplyRatingColor) { el.style.color = ratingColors[value]; }
      el.style.visibility = window.getComputedStyle(visibileRatingButtons[i]).getPropertyValue('visibility');
    }
    el.childNodes[0].textContent = visibileRatingButtons[i].childNodes[0].textContent;
  });
};

const gatherReplies = () =>
{
  [...replyContainers].forEach(el => { el.remove(); });

  let replyContainer = document.createElement('div');
  replyContainer.id = 'injected-reply-container'
  replyContainer.classList.add(cls.Chat, "reply-container-inject");

  document.body.appendChild(replyContainer);

  let closeButton = document.createElement('Button');
  closeButton.innerHTML = "X";
  closeButton.classList.add(cls.Chat,"btn","border");
  closeButton.style = "margin-left:0.75em; padding:0.2em";
  closeButton.addEventListener("click", () => { replyContainer.remove(); });
  replyContainer.appendChild(closeButton);

  // let replies = [...document.querySelectorAll('div.markdown-wrapper-last-msg')]; // 'div.char-msg'
  let replies = []; // What doesn't Array.from work ????
  for (let i = 0; i < recentReplies.length; i++) {
    if (!replies.find(el => el.textContent === recentReplies[i].textContent)) {
      replies.push(recentReplies[i]); // Don't push duplicates
    }
  }
  currentReplies = replies;

  replies.forEach(el => {
    const originalRatings = (el.parentElement).querySelector('div.align-items-center');
    let ratingButtons = originalRatings.cloneNode(true);
    let ratingText = ratingButtons.childNodes[4];
    let originalButtons = [...originalRatings.childNodes]
    let message = el.cloneNode(true);
    let messageBtn = document.createElement("Button");

    ratingButtons.style = "padding-left:1em;"
    ratingButtons.childNodes.forEach( (el,i) => {
      if (el.tagName == 'BUTTON'){
        el.onclick = () => { ratingText.style.visibility = 'hidden'; replyClick(message.textContent); ratingClick(el.title, el);};
        el.addEventListener("mouseenter", () => { messageBtn.style.color = `${scriptConfig.ReplyWindow.replyHighlightColor}`; });
        el.addEventListener("mouseleave", () => { messageBtn.style.color = ''; });
        el.style = "padding-top:0em; padding-bottom:0em;";
      }
      else if (el.tagName == 'DIV') {
        el.style = "visibility:hidden; font-weight:bold;";
      }
    });
    //ratingButtons.style = "padding:0em;margin:0em";

    message.classList.remove('markdown-wrapper-last-msg');
    message.classList.add(cls.Chat, "border");
    message.style = "padding:0.5em; padding-bottom:0em; border-radius: 0.8em;";

    messageBtn.onclick = () => {replyClick(message.textContent)};
    messageBtn.appendChild(message);
    messageBtn.classList.add("chat-inject","btn");
    messageBtn.style = "width:100%; text-align:left;";
    messageBtn.addEventListener("mouseenter", () => { messageBtn.style.color = `${scriptConfig.ReplyWindow.replyHighlightColor}`; });
    messageBtn.addEventListener("mouseleave", () => { messageBtn.style.color = ''; });
    messageBtn.childNodes.forEach( (el) => { el.addEventListener("mouseleave", () => { messageBtn.style.color = ''; }); });

    replyContainer.appendChild(messageBtn);
    replyContainer.appendChild(ratingButtons);
    // Monitor overflow and change style to make scrollable
    let overflow = replyContainer.offsetHeight >= window.innerHeight ? 'overflow: scroll; height: 100%;' : '';
    replyContainer.style = `${overflow} bottom:0; ${scriptConfig.ReplyWindow.replyContainerPosition}:0; position:absolute;
    width:${scriptConfig.ReplyWindow.replyContainerWidth}%; padding-bottom:1em; z-index: 1`
  });
}

const removeReplies = () =>
{
  const checkReplyContainer = document.getElementById('injected-reply-container');
  if (checkReplyContainer) { checkReplyContainer.remove(); };
}

const createChatElements = () =>
{
  let button = document.createElement("Button");
  button.innerHTML = "⚙";
  button.classList.add("chat-inject","btn","border");
  button.style = "top:0;right:0;position:absolute;z-index: 1025"
  button.addEventListener("click", archiveChat);
  document.body.appendChild(button);
}

const setupChatTools = () =>
{
  createChatElements();
  VM.shortcut.register(hotkey.Chat.DownloadChat, () => { archiveChat(); });
  VM.shortcut.register(hotkey.Chat.DisplayChatCharImage, () => { toggleChatCharImage(); });
  VM.shortcut.register(hotkey.Chat.DisplayChatSceneImage, () => { toggleSceneImage(); });
  VM.shortcut.register(hotkey.Chat.ClickRetry, () => { clickDropdownElement('Regenerate Last Message', true); });
  VM.shortcut.register(hotkey.Chat.ClickRemove, () => { clickDropdownElement('Remove Messages'); });
  VM.shortcut.register(hotkey.Chat.ShowConfig, () => { showConfigMenu(tabLabel.config); });
  VM.shortcut.enable();
}

const removeChatTools = () =>
{
  const chatElements = document.querySelectorAll('.chat-inject');
  chatElements.forEach(el => {
    el.remove();
  });
}

const setupEditTools = () =>
{
  VM.shortcut.register(hotkey.Edit.EditSave, () => {
    [...document.querySelectorAll('button[type="submit"]')].find(el => el.textContent === 'Save ').click();
  });
  VM.shortcut.enable();
}

let replyView = document.getElementsByClassName('annotation-buttons-container');
let typingDots = document.getElementsByClassName('typing-dot');

const checkChatMutation = () =>
{
  // console.log("Chat Mutated!");
  // Check for new replies, update container if enabled
  if (replyView.length > 2) {
    let recentReply = recentReplies[0];
    if (scriptConfig.ReplyWindow.showChatReplyWindow && !currentReplies.find(el => el.textContent === recentReply.textContent)) {
      console.log("New Replies!!! -> " + replyView.length)
      docObserver.disconnect();
      setTimeout(() => {
          gatherReplies();
          console.log("Refreshing Replies...");
        attachDocObserver();
      }, 1000);
    }
  }
}

const checkChatElements = () =>
{
  if (scriptConfig.Chat.showChatCharImage && charNameTitle.length > 0 && scriptConfig.Chat.chatCharImageSize >= 80) {
    setTimeout(() => { createChatCharImage(); }, 1000);
  }
  if (scriptConfig.Chat.showChatSceneImage && scriptConfig.Chat.chatSceneImageSize >= 80) {
    if (!document.getElementById('inject-chat-scene-image')) {
      createChatSceneImage();
    }
  }
  if(typingDots.length > 0) { // Receiving response
    removeReplies();
    // TODO: REGEX previous AI message here
  }

}

const checkUrlMutation = (url) =>
{
    // Chat pages
    if (url.indexOf("/chat?char=") > -1 || url.indexOf("/chat?hist=") > -1) {
      setupChatTools()
    }
    else {
      removeChatTools()
    }
    // Editing Page
    if (url.indexOf("/editing?cha") > -1) {
      setupEditTools()
    }

    // Convenience hacks
    if (scriptConfig.Misc.enableConvenienceHacks && url.indexOf("/chat?hist=") > -1){
      // Click auto button in Chat Room
      setTimeout(() => {  document.querySelector('button.align-items-center').click(); }, 1500);
    }
}

let previousUrl = '';
let prevMessageCount = 0;
const config = {subtree: true, childList: true};
const docObserver = new MutationObserver( (mutations) =>
{
  console.log("Doc Mutated!");
  // Observe chat elements
  if (location.href.indexOf("/chat?char=") > -1) {
    checkChatElements();
  }
  // Observe Chat
  if (document.getElementById('scrollBar')) {
    // docObserver.disconnect();
    checkChatMutation();
    // setTimeout(() => {  checkUrlMutation(location.href); attachDocObserver(); }, 1000);
  }
  // Observe Url changes
  if (location.href !== previousUrl) {
    docObserver.disconnect();
    checkUrlMutation(location.href);
    setTimeout(() => { attachDocObserver(); }, 1000);
  }
  previousUrl = location.href;
});

const attachDocObserver = () =>
{
  console.log("Doc MutationObserver Enabled!");
  docObserver.observe(document.body, config);
}

attachDocObserver();
})();